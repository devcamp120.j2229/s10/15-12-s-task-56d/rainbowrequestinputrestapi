package com.devcamp.api.rainbowapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public ArrayList<String> getRainbowColors(String color) {
        ArrayList<String> listColors = new ArrayList<>();

        for (String rainbow : rainbows) {
            if (rainbow.contains(color)) {
                listColors.add(rainbow);
            }
        }
        return listColors;
    }

    public String getRainbowColor(int index) {
        String result = "";
        if (index >=0 && index <=6) {
            result = rainbows[index];
        }

        return result;
    }
}
