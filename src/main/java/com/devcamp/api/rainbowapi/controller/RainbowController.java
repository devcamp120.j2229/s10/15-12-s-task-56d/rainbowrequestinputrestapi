package com.devcamp.api.rainbowapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.rainbowapi.service.RainbowService;

@CrossOrigin
@RestController
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> getRainbowColors(@RequestParam(value = "rainbow", defaultValue = "") String color) {
        //RainbowService rainbowService = new RainbowService();
        return rainbowService.getRainbowColors(color);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getRainbowColor(@PathVariable(name = "index") int ind) {
        return rainbowService.getRainbowColor(ind);
    }

    @GetMapping("/rainbow-request-param")
    public String getRainbowColor2() {
        return rainbowService.getRainbowColor(0);
    }
}
